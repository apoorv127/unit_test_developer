class Employee:
    def __init__(self, company, role):
        if company is None or role is None:
            raise NameError('No Inputs Provided for Employee')
        if not isinstance(company, str) or not isinstance(role, str):
            raise TypeError('All of your Inputs for Employee must be string')

        self.company = company
        self.role = role



