from classes.employee import Employee


class Developer(Employee):
    def __init__(self, name,  company, role='Developer'):
        if name is None or company is None:
            raise NameError('No Inputs Provided for Developer')

        super().__init__(company, role)
        self.name = name
        self.skills = []

    def addSkill(self, skill):
        self.skills.append(skill)

    def editCompany(self, company):
        self.company = company

