import unittest
from classes.developer import Developer
from classes.skill import Skill


class DeveloperTestCaseWithSetup(unittest.TestCase):
    def setUp(self):
        self.developer1 = Developer('spiderman', 'spidey', 'Game Developer')
        self.developer1.addSkill(Skill('C#'))
        self.developer1.addSkill(Skill('Unity'))
        self.developer1.addSkill(Skill('Photoshop'))
        self.developer1.addSkill(Skill('Maya'))

        self.developer2 = Developer('batman', 'gotham', 'Data Scientist')

    def test_developer_skill_sets_valid(self):
        actual = []

        for skill in self.developer1.skills:
            actual.append(skill.techStack)
            
        expected = ['C#', 'Unity', 'Photoshop', 'Maya']

        self.assertListEqual(actual, expected)

    def test_developer_skill_sets_invalid(self):
        actual = []

        for skill in self.developer1.skills:
            actual.append(skill.techStack)

        expected = ['C#', 'Unity', 'Python', 'Maya']

        self.assertNotEqual(actual, expected)
        
    def test_developer_no_skill_sets_valid(self):
        self.assertEqual(len(self.developer2.skills), 0)

    def test_developer_no_skill_sets_invalid(self):
        self.assertNotEqual(len(self.developer2.skills), 100)

    def test_developer_add_new_skills_valid(self):
        self.developer2.addSkill(Skill('Linear Algebra'))
        self.developer2.addSkill(Skill('Statistics'))
        
        actual = []
        
        for skill in self.developer2.skills:
            actual.append(skill.techStack)
        
        expected = ['Linear Algebra', 'Statistics']
        
        self.assertListEqual(actual, expected)
            

    def test_developer_count_skills_valid(self):
        actual = []

        for skill in self.developer1.skills:
            actual.append(skill.techStack)
        
        self.assertEqual(len(actual), 4)

    def test_developer_add_skill_valid(self):
        # Before addSkill
        actual = []

        for skill in self.developer1.skills:
            actual.append(skill.techStack)

        self.assertEqual(len(actual), 4)

        # After addSkill
        self.developer1.addSkill(Skill('Python'))

        actual.append(self.developer1.skills[len(self.developer1.skills) - 1].techStack)
        # print(actual)

        self.assertEqual(len(actual), 5)
