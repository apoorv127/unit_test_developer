import unittest
from classes.developer import Developer
from classes.skill import Skill


class DeveloperSkillsTestCaseWithSetup(unittest.TestCase):
    def setUp(self):
        pass

    def test_with_all_arguments_invalid_developer_init(self):
        with self.assertRaises(TypeError) as context1:
            self.developer1 = Developer(232, 232, [])
        self.assertEqual(str(context1.exception), 'All of your Inputs for Employee must be string')

    def test_with_name_role_argument_invalid_developer_init(self):
        with self.assertRaises(TypeError) as context2:
            self.developer2 = Developer(232, 'uwm', [])
        self.assertEqual(str(context2.exception), 'All of your Inputs for Employee must be string')

    def test_with_role_argument_invalid_developer_init(self):
        with self.assertRaises(TypeError) as context3:
            self.developer3 = Developer('batman', 'gotham', [])
        self.assertEqual(str(context3.exception), 'All of your Inputs for Employee must be string')

    def test_with_name_input_none_developer_init(self):
        with self.assertRaises(NameError) as context4:
            self.developer4 = Developer(None, 'gotham', 'Developer')
        self.assertEqual(str(context4.exception), 'No Inputs Provided for Developer')

    def test_with_name_company_input_none_developer_init(self):
        with self.assertRaises(NameError) as context5:
            self.developer5 = Developer(None, None)
        self.assertEqual(str(context5.exception), 'No Inputs Provided for Developer')



    def test_skill_instance_valid(self):
        self.developer1 = Developer('spiderman', 'spidy', 'Game Developer')
        self.developer1.skills.append(Skill('Java'))
        self.assertIsInstance(self.developer1.skills[0], Skill)

    def test_skill_instance_invalid(self):
        self.developer1 = Developer('spiderman', 'spidy', 'Game Developer')
        self.developer1.skills.append('Java')
        self.assertNotIsInstance(self.developer1.skills[0], Skill)


